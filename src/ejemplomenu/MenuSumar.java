package ejemplomenu;

import java.util.Scanner;

public class MenuSumar implements Menu {

    private final Scanner scan = new Scanner(System.in);
    private int operando1, operando2;
    
    @Override
    public void procesar() {
        int cmd;
        do {
            presentar();
            cmd = scan.nextInt();
            scan.nextLine(); // consume el fin de linea
            switch (cmd) {
                case 1: introducirOperando1(); break;
                case 2: introducirOperando2(); break;
                case 3: mostrarResultado(); break;
            }
        } while (cmd != 0);
    }
    
    private void presentar() {
        System.out.println("OPCION SUMAR");
        System.out.println("============");
        System.out.println("[1]\tIntroducir primer operando");
        System.out.println("[2]\tIntroducir segundo operando");
        System.out.println("[3]\tMostrar resultado");
        System.out.println();
        System.out.println("[0]\t Volver");
        System.out.print(">");
    }
    
    private void introducirOperando1() {
        System.out.print(">Primer operando: ");
        this.operando1 = scan.nextInt();
        scan.nextLine(); // consume el fin de linea
    }
    
    private void introducirOperando2() {
        System.out.print(">Segundo operando: ");
        this.operando2 = scan.nextInt();
        scan.nextLine(); // consume el fin de linea
    }
    
    private void mostrarResultado() {
        int suma = operando1 + operando2;
        System.out.println("El resultado de " + operando1 + " + " + operando2 + " = " + suma);
        System.out.println("---");
    }
}
