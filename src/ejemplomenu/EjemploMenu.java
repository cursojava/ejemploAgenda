package ejemplomenu;

import ejemplomenu.agenda.MenuAgenda;
import java.sql.SQLException;
import java.util.Scanner;

public class EjemploMenu implements Menu {

    private static final String DATABASE_URI = "jdbc:derby:agenda;create=true";
    
    private final Scanner scan = new Scanner(System.in);
    
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        //Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        try(java.sql.Connection conn = java.sql.DriverManager.getConnection(DATABASE_URI)) {
            Menu app = new MenuAgenda(); //new EjemploMenu();
            app.procesar();
        }
    }

    @Override
    public void procesar() {
        int cmd;
        do {
            presentar();
            cmd = scan.nextInt();
            switch (cmd) {
                case 1: opcion1(); break;
                case 2: opcion2(); break;
                case 3: opcion3(); break;
            }
        } while (cmd != 0);
    }
    
    private void presentar() {
        System.out.println("Ejemplo Menú");
        System.out.println("============");
        System.out.println("[1]\tOpción saludar.");
        System.out.println("[2]\tOpción sumar.");
        System.out.println("[3]\tOpción agenda.");
        System.out.println();
        System.out.println("[0]\tSalir");
        System.out.print(">");
    }
    
    private void opcion1() {
        Menu menu = new MenuSaludar();
        menu.procesar();
    }
    
    private void opcion2() {
        Menu menu = new MenuSumar();
        menu.procesar();
    }
    
    private void opcion3() {
        Menu menu = new MenuAgenda();
        menu.procesar();
    }
}
