package ejemplomenu.agenda;

import ejemplomenu.agenda.dao.ContactoDAO;
import ejemplomenu.agenda.dao.DAOFactory;
import java.util.List;

public class ContactoService {

    private static final ContactoService SERVICE = new ContactoService();
    
    private final ContactoDAO contactoDao;
    
    public static ContactoService getInstance() {
        return SERVICE;
    }
    
    private ContactoService() {
        this.contactoDao = DAOFactory.getInstance().getContactoDAO();
    }
    
    public List<Contacto> getContactos() {
        return contactoDao.getContactos();
    }
    
    public Contacto findContacto(int codigo) {
        return contactoDao.findContacto(codigo);
    }
    
    public List<Contacto> findContacto(String nombre) {
        return contactoDao.findContacto(nombre);
    }
    
    public Contacto addContacto(String nombre, String direccion) {
        return contactoDao.addContacto(nombre, direccion);
    }
    
    public Contacto updateContacto(Contacto contacto) {
        return contactoDao.updateContacto(contacto);
    }
}
