package ejemplomenu.agenda;

import ejemplomenu.Menu;
import java.util.List;
import java.util.Scanner;

public class MenuAgenda implements Menu {

    private final Scanner scan = new Scanner(System.in);
    private final ContactoService servicio = ContactoService.getInstance();
    
    public MenuAgenda() {
    }
    
    @Override
    public void procesar() {
        int cmd;
        do {
            presentar();
            cmd = scan.nextInt();
            scan.nextLine(); // consume el fin de linea
            switch (cmd) {
                case 1: opcionContactos(); break;
                case 2: opcionBuscarContacto(); break;
                case 3: opcionAñadirContacto(); break;
                case 4: opcionEditarContacto(); break;
            }
        } while (cmd != 0);
    }
    
    private void presentar() {
        System.out.println("Menú Agenda");
        System.out.println("===========");
        System.out.println("[1]\tMostrar todos los contactos");
        System.out.println("[2]\tBuscar contacto");
        System.out.println("[3]\tAñadir contacto");
        System.out.println("[4]\tModificar contacto");
        System.out.println();
        System.out.println("[0]\tVolver");
        System.out.print(">");
    }
    
    private void opcionContactos() {
        List<Contacto> contactos = servicio.getContactos();
        if (contactos.isEmpty()) {
            System.out.println("\t[No existen contactos]");
        } else {
            System.out.println("RELACIÓN DE CONTACTOS:");
            System.out.println("---------------------");
            for (Contacto contacto: contactos) {
                System.out.println("\t " + contacto.getCodigo() + ".- " + contacto.getNombre());
            }
        }
        System.out.println("---");
    }
    
    private void opcionBuscarContacto() {
        System.out.println("BUSCAR CONTACTO:");
        System.out.println("---------------");
        System.out.print("Introduzca el código del contacto a buscar: ");
        int codigo = scan.nextInt(); scan.nextLine();
        Contacto contacto = servicio.findContacto(codigo);
        if (contacto != null) {
            System.out.println("CONTACTO ENCONTRADO:");
            System.out.println("Nombre: " + contacto.getNombre());
            System.out.println("Direccion: " + contacto.getDireccion());
        } else {
            System.out.println("CONTACTO NO ENCONTRADO.");
        }
        System.out.println("---");
    }
    
    private void opcionAñadirContacto() {
        System.out.println("NUEVO CONTACTO:");
        System.out.println("--------------");
        System.out.print("Nombre: ");
        String nombre = scan.nextLine();
        System.out.print("Direccion: ");
        String direccion = scan.nextLine();
        servicio.addContacto(nombre, direccion);
        System.out.println("Contacto añadido correctamente");
        System.out.println("---");
    }
    
    private void opcionEditarContacto() {
        System.out.println("EDITAR CONTACTO:");
        System.out.println("---------------");
        System.out.print("Introduzca el código del contacto a editar: ");
        int codigo = scan.nextInt(); scan.nextLine();
        Contacto contacto = servicio.findContacto(codigo);
        if (contacto != null) {
            System.out.println("(introduzca cadena vacía para no modificar un valor)");
            System.out.print("Nombre [" + contacto.getNombre() + "]: ");
            String nombre = scan.nextLine();
            if (!"".equals(nombre)) contacto.setNombre(nombre);
            System.out.print("Direccion [" + contacto.getDireccion() + "]: ");
            String direccion = scan.nextLine();
            if (!"".equals(direccion)) contacto.setDireccion(direccion);
            servicio.updateContacto(contacto);
            System.out.println("Contacto modificado correctamente");
        } else {
            System.out.println("Contacto no encontrado");
        }
        System.out.println("---");
    }
}