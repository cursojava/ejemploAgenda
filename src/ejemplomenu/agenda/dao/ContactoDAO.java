package ejemplomenu.agenda.dao;

import ejemplomenu.agenda.Contacto;
import java.util.List;

public interface ContactoDAO {
 
    public List<Contacto> getContactos();
    public Contacto findContacto(int codigo);
    public List<Contacto> findContacto(String nombre);
    public Contacto addContacto(String nombre, String direccion);
    public Contacto updateContacto(Contacto contacto);
}