package ejemplomenu.agenda.dao;

public abstract class DAOFactory {
 
    //private static final DAOFactory INSTANCE = new ejemplomenu.agenda.dao.derby.DerbyDAOFactory();
    private static final DAOFactory INSTANCE = new ejemplomenu.agenda.dao.simple.SimpleDAOFactory();
    
    public static DAOFactory getInstance() {
        return INSTANCE;
    }
    
    protected DAOFactory() {}
    
    public abstract ContactoDAO getContactoDAO();
}