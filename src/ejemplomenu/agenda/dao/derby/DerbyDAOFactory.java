package ejemplomenu.agenda.dao.derby;

import ejemplomenu.agenda.dao.simple.SimpleContactoDAO;
import ejemplomenu.agenda.dao.DAOFactory;

public class DerbyDAOFactory extends DAOFactory {
 
    @Override
    public SimpleContactoDAO getContactoDAO() {
        return new SimpleContactoDAO();
    }
}
