package ejemplomenu.agenda.dao.simple;

import ejemplomenu.agenda.dao.ContactoDAO;
import ejemplomenu.agenda.dao.DAOFactory;

public class SimpleDAOFactory extends DAOFactory {
    
    @Override
    public ContactoDAO getContactoDAO() {
        return new SimpleContactoDAO();
    }
}