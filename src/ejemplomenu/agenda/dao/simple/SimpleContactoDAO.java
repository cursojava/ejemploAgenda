package ejemplomenu.agenda.dao.simple;

import ejemplomenu.agenda.Contacto;
import ejemplomenu.agenda.dao.ContactoDAO;
import java.util.List;

public class SimpleContactoDAO implements ContactoDAO {
    
    private static int nextCodigo = 1;
    
    private List<Contacto> contactos;
    
    public SimpleContactoDAO() {}
    
    @Override
    public List<Contacto> getContactos() {
        if (contactos == null) {
            contactos = new java.util.ArrayList<>();
        }
        return contactos;
    }
    
    @Override
    public Contacto findContacto(int codigo) {
        Contacto found = null;
        for (Contacto contacto: getContactos()) {
            if (contacto.getCodigo() == codigo) {
                found = contacto;
                break;
            }
        }
        return found;
    }
    
    @Override
    public List<Contacto> findContacto(String nombre) {
        List<Contacto> result = new java.util.ArrayList<>();
        for (Contacto contacto: getContactos()) {
            if (contacto.getNombre().contains(nombre)) {
                result.add(contacto);
                break;
            }
        }
        return result;
    }
    
    @Override
    public Contacto addContacto(String nombre, String direccion) {
        Contacto contacto = new Contacto(nextCodigo++, nombre, direccion);
        getContactos().add(contacto);
        return contacto;
    }
    
    @Override
    public Contacto updateContacto(Contacto contacto) {
        Contacto found = findContacto(contacto.getCodigo());
        if (found != null) {
            found.setNombre(contacto.getNombre());
            found.setDireccion(contacto.getDireccion());
        }
        return found;
    }
}