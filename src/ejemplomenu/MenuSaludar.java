package ejemplomenu;

import java.util.Scanner;

public class MenuSaludar implements Menu {
    
    private final Scanner scan = new Scanner(System.in);
    
    @Override
    public void procesar() {
        int cmd;
        do {
            presentar();
            cmd = scan.nextInt();
            scan.nextLine(); // consume el fin de linea
            switch (cmd) {
                case 1: saludar(); break;
            }
        } while (cmd != 0);
    }
    
    private void presentar() {
        System.out.println("OPCION SALUDAR");
        System.out.println("==============");
        System.out.println("[1]\tSaludar.");
        System.out.println();
        System.out.println("[0]\t Volver");
        System.out.print(">");
    }
    
    private void saludar() {
        System.out.println("Hola Mundo!");
    }
}
